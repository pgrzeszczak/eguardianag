package pl.put.siwoz.eguardian.guard;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class MainFragment extends Fragment {

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		((MainActivity) activity).onSectionAttached(0);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_main,
				container, false);
		return rootView;
	}

	@Override
	public void onResume() {
		super.onResume();
		TextView text = (TextView) getView().findViewById(R.id.chosenSenior);
		if (MainActivity.seniorLogin != null) {
			text.setText(MainActivity.seniorLogin);
		}
	}
}
