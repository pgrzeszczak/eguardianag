package pl.put.siwoz.eguardian.guard.entities;

public class MealEvent extends Event {
	private int mealId;
	private String name;
	private String description;

	public int getMealId() {
		return mealId;
	}

	public void setMealId(int mealId) {
		this.mealId = mealId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Override
	public String getSomethingNamelike() {
		return getName();
	}
}
