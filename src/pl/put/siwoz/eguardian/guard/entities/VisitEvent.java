package pl.put.siwoz.eguardian.guard.entities;

public class VisitEvent extends Event {
	private int visitId;
	private int placeId;
	private String name;
	private String doctorName;

	public int getVisitId() {
		return visitId;
	}

	public void setVisitId(int id) {
		this.visitId = id;
	}

	public int getPlaceId() {
		return placeId;
	}

	public void setPlaceId(int placeId) {
		this.placeId = placeId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDoctorName() {
		return doctorName;
	}

	public void setDoctorName(String doctorName) {
		this.doctorName = doctorName;
	}

	@Override
	public String getSomethingNamelike() {
		return getName();
	}

}
