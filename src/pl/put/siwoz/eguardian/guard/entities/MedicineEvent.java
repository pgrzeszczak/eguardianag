package pl.put.siwoz.eguardian.guard.entities;

public class MedicineEvent extends Event {
	private int medicineId;
	private int categoryId;
	private String name;
	private String dose;

	public int getMedicineId() {
		return medicineId;
	}

	public void setMedicineId(int id) {
		this.medicineId = id;
	}

	public int getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(int categoryId) {
		this.categoryId = categoryId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDose() {
		return dose;
	}

	public void setDose(String dose) {
		this.dose = dose;
	}

	@Override
	public String getSomethingNamelike() {
		return getName();
	}

}
