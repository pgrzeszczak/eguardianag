package pl.put.siwoz.eguardian.guard.entities;

import java.sql.Date;
import java.util.ArrayList;
import java.util.Collections;

public abstract class Event {
	private int id;
	private int somethingId;
	private int seniorId;
	private int guardianId;
	private Date startTime = new Date(1401861600);
	private Date endTime = new Date(1401861600);
	private int interval;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getSomethingId() {
		return somethingId;
	}

	public void setSomethingId(int eventId) {
		this.somethingId = eventId;
	}

	public int getSeniorId() {
		return seniorId;
	}

	public void setSeniorId(int seniorId) {
		this.seniorId = seniorId;
	}

	public int getGuardianId() {
		return guardianId;
	}

	public void setGuardianId(int guardianId) {
		this.guardianId = guardianId;
	}

	public Date getStartTime() {
		return startTime;
	}

	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}

	public Date getEndTime() {
		return endTime;
	}

	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}

	public int getInterval() {
		return interval;
	}

	public void setInterval(int interval) {
		this.interval = interval;
	}

	public String getSomethingNamelike() {
		return "";
	}

	public String getHumanReadableInterval() {
		if (interval == 0)
			interval = 1;
		if (interval % 60 == 0) {
			if (1440 % interval == 0) {
				return "Codziennie o godzinie " + getHours() + ".";
			}
			return "Co " + interval / 60 + " godzin.";
		}
		return "Co " + interval + " minut.";
	}

	@SuppressWarnings({ "deprecation" })
	private String getHours() {
		String result = "";
		int hourInterval = interval / 60;
		int count = 24 / hourInterval;

		String minutes = "00";// startTime.getMinutes();
		ArrayList<Integer> hours = new ArrayList<Integer>();

		for (int x = 0; x < count; x++) {
			// hours.add((startTime.getHours() + hourInterval * x) % 24);
			hours.add((8 + hourInterval * x) % 24);
		}
		Collections.sort(hours);

		for (int x = 0; x < count; x++) {
			result += hours.get(x) + ":" + minutes;
			if (count > 1 && x + 1 < count) {
				if (x + 2 == count) {
					result += " i ";
				} else {
					result += ", ";
				}
			}
		}

		return result;
	}
}
