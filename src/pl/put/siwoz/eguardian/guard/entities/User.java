package pl.put.siwoz.eguardian.guard.entities;

public class User {
	private String login;
	private String firstname;
	private String lastname;

	public String getLogin() {
		return login;
	}

	public User setLogin(String login) {
		this.login = login;
		return this;
	}

	@Override
	public String toString() {
		return login;
	}

}
