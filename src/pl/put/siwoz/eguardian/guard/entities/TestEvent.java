package pl.put.siwoz.eguardian.guard.entities;

public class TestEvent extends Event {
	private int testId;
	private int categoryId;
	private String name;

	public int getTestId() {
		return testId;
	}

	public void setTestId(int id) {
		this.testId = id;
	}

	public int getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(int categoryId) {
		this.categoryId = categoryId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String getSomethingNamelike() {
		return getName();
	}

}
