package pl.put.siwoz.eguardian.guard;

import pl.put.siwoz.eguardian.guard.backend.BackendService;
import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;

public class StartActivity extends Activity {
	private UserLoginTask mAuthTask = null;

	public class UserLoginTask extends AsyncTask<Void, Void, Boolean> {
		private Activity activity;
		private BackendService backendService;

		public void setActivity(Activity activity) {
			this.activity = activity;
		}

		@Override
		protected Boolean doInBackground(Void... params) {
			backendService = new BackendService(activity);
			boolean accountExists = false;

			AccountManager accountManager = (AccountManager) getSystemService(ACCOUNT_SERVICE);
			if (accountManager.getAccountsByType(getString(R.string.auth_type)).length > 0) {
				Account mAccount = accountManager
						.getAccountsByType(getString(R.string.auth_type))[0];

				backendService.setCredentials(mAccount.name,
						accountManager.getPassword(mAccount));
				accountExists = (backendService.loginGuardian() == BackendService.LOGIN_SUCCESSFUL);
				if (accountExists) MainActivity.guardianLogin = mAccount.name;
			}

			if (accountExists) {
				return true;
			} else {
				return false;
			}
		}

		@Override
		protected void onPostExecute(final Boolean success) {
			Class<? extends Activity> activityClass;

			if (success) {
				activityClass = MainActivity.class;
			} else {
				activityClass = FirstLaunchActivity.class;
			}
			startSth(activityClass);
		}
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		mAuthTask = new UserLoginTask();
		mAuthTask.setActivity(this);
		mAuthTask.execute((Void) null);
	}

	public void startSth(Class<? extends Activity> activityClass) {
		startActivity(new Intent(this, activityClass));
		finish();

	}
}
