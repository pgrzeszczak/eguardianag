package pl.put.siwoz.eguardian.guard;

import java.util.concurrent.atomic.AtomicInteger;

import android.app.ActionBar;
import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.gcm.GoogleCloudMessaging;

public class MainActivity extends Activity implements
		NavigationDrawerFragment.NavigationDrawerCallbacks {

	/**
	 * Fragment managing the behaviors, interactions and presentation of the
	 * navigation drawer.
	 */
	private NavigationDrawerFragment mNavigationDrawerFragment;

	public static String guardianLogin = null;
	public static String seniorLogin = null;

	/**
	 * Used to store the last screen title. For use in
	 * {@link #restoreActionBar()}.
	 */
	private CharSequence mTitle;

	private final static int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;

	private static final String TAG = "MainActivity";

	GoogleCloudMessaging gcm;
	AtomicInteger msgId = new AtomicInteger();
	SharedPreferences prefs;
	Context context;

	String regid;

	private AddEventFragment addEventFragment;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		mNavigationDrawerFragment = (NavigationDrawerFragment) getFragmentManager()
				.findFragmentById(R.id.navigation_drawer);
		mTitle = getTitle();

		// Set up the drawer.
		mNavigationDrawerFragment.setUp(R.id.navigation_drawer,
				(DrawerLayout) findViewById(R.id.drawer_layout));
		context = getApplicationContext();

		// Check device for Play Services APK.
		checkPlayServices();

	}

	@Override
	protected void onResume() {
		super.onResume();
		checkPlayServices();
		mNavigationDrawerFragment.selectItem(0);
	}

	/**
	 * Sends the registration ID to your server over HTTP, so it can use
	 * GCM/HTTP or CCS to send messages to your app. Not needed for this demo
	 * since the device sends upstream messages to a server that echoes back the
	 * message using the 'from' address in the message.
	 */
	private void sendRegistrationIdToBackend() {
		// Your implementation here.
		Log.i(TAG, "New registration id obtained: " + regid);
	}

	public void setCurrentAddEventFragment(AddEventFragment fragment) {
		this.addEventFragment = fragment;
	}

	public void addMeal(View view) {
		((AddMealEventFragment) addEventFragment).addMeal(view);
	}

	public void deleteMeal(View view) {
		((AddMealEventFragment) addEventFragment).deleteMeal(view);
	}

	public void createNewMealEvent(View view) {
		openAddMealFragment();
	}

	public void openMealListFragment() {
		FragmentManager fragmentManager = getFragmentManager();
		fragmentManager.beginTransaction()
				.replace(R.id.container, new MealEventListFragment()).addToBackStack("").commit();
	}

	public void openAddMealFragment() {
		addEventFragment = new AddMealEventFragment();
		FragmentManager fragmentManager = getFragmentManager();
		fragmentManager.beginTransaction()
				.replace(R.id.container, addEventFragment).addToBackStack("").commit();
	}

	public void addMedicine(View view) {
		((AddMedicineEventFragment) addEventFragment).addMedicine(view);
	}

	public void deleteMedicine(View view) {
		((AddMedicineEventFragment) addEventFragment).deleteMedicine(view);
	}

	public void createNewMedicineEvent(View view) {
		openAddMedicineFragment();
	}

	public void openMedicineListFragment() {
		FragmentManager fragmentManager = getFragmentManager();
		fragmentManager.beginTransaction()
				.replace(R.id.container, new MedicineEventListFragment()).addToBackStack("")
				.commit();
	}

	public void openAddMedicineFragment() {
		addEventFragment = new AddMedicineEventFragment();
		FragmentManager fragmentManager = getFragmentManager();
		fragmentManager.beginTransaction()
				.replace(R.id.container, addEventFragment).addToBackStack("").commit();
	}

	public void addTest(View view) {
		((AddTestEventFragment) addEventFragment).addTest(view);
	}

	public void deleteTest(View view) {
		((AddTestEventFragment) addEventFragment).deleteTest(view);
	}

	public void createNewTestEvent(View view) {
		openAddTestFragment();
	}

	public void openTestListFragment() {
		FragmentManager fragmentManager = getFragmentManager();
		fragmentManager.beginTransaction()
				.replace(R.id.container, new TestEventListFragment()).addToBackStack("").commit();
	}

	public void openAddTestFragment() {
		addEventFragment = new AddTestEventFragment();
		FragmentManager fragmentManager = getFragmentManager();
		fragmentManager.beginTransaction()
				.replace(R.id.container, addEventFragment).addToBackStack("").commit();
	}

	public void addVisit(View view) {
		((AddVisitEventFragment) addEventFragment).addVisit(view);
	}

	public void deleteVisit(View view) {
		((AddVisitEventFragment) addEventFragment).deleteVisit(view);
	}

	public void createNewVisitEvent(View view) {
		openAddVisitFragment();
	}

	public void openVisitListFragment() {
		FragmentManager fragmentManager = getFragmentManager();
		fragmentManager.beginTransaction()
				.replace(R.id.container, new VisitEventListFragment()).addToBackStack("").commit();
	}

	public void openAddVisitFragment() {
		addEventFragment = new AddVisitEventFragment();
		FragmentManager fragmentManager = getFragmentManager();
		fragmentManager.beginTransaction()
				.replace(R.id.container, addEventFragment).addToBackStack("").commit();
	}

	/**
	 * Check the device to make sure it has the Google Play Services APK. If it
	 * doesn't, display a dialog that allows users to download the APK from the
	 * Google Play Store or enable it in the device's system settings.
	 */
	private boolean checkPlayServices() {
		int resultCode = GooglePlayServicesUtil
				.isGooglePlayServicesAvailable(this);
		if (resultCode != ConnectionResult.SUCCESS) {
			if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
				GooglePlayServicesUtil.getErrorDialog(resultCode, this,
						PLAY_SERVICES_RESOLUTION_REQUEST).show();
			} else {
				Log.i(TAG, "This device is not supported.");
				finish();
			}
			return false;
		}
		return true;
	}

	@Override
	public void onNavigationDrawerItemSelected(int position) {
		// update the main content by replacing fragments
		if (position > 0 && seniorLogin == null) {
			Toast.makeText(context,
					getString(R.string.error_senior_not_chosen),
					Toast.LENGTH_LONG).show();
			mNavigationDrawerFragment.selectItem(0);
		} else {
			if (position == 0) {
				FragmentManager fragmentManager = getFragmentManager();
				fragmentManager
						.beginTransaction()
						.replace(R.id.container,
								new MainFragment()).addToBackStack("")
						.commit();
				onSectionAttached(0);
			} else if (position == 1) {
				openMealListFragment();
				onSectionAttached(1);
			} else if (position == 2) {
				openMedicineListFragment();
				onSectionAttached(2);
			} else if (position == 3) {
				openTestListFragment();
				onSectionAttached(3);
			} else if (position == 4) {
				openVisitListFragment();
				onSectionAttached(4);
			}
		}
	}

	public void onSectionAttached(int number) {
		switch (number) {
		case 0:
			mTitle = getString(R.string.title_section_main);
			break;
		case 1:
			mTitle = getString(R.string.title_section_meal);
			break;
		case 2:
			mTitle = getString(R.string.title_section_medicine);
			break;
		case 3:
			mTitle = getString(R.string.title_section_test);
			break;
		case 4:
			mTitle = getString(R.string.title_section_visit);
			break;
		}
	}

	public void restoreActionBar() {
		ActionBar actionBar = getActionBar();
		actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
		actionBar.setDisplayShowTitleEnabled(true);
		actionBar.setTitle(mTitle);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		if (!mNavigationDrawerFragment.isDrawerOpen()) {
			// Only show items in the action bar relevant to this screen
			// if the drawer is not showing. Otherwise, let the drawer
			// decide what to show in the action bar.
			getMenuInflater().inflate(R.menu.main, menu);
			restoreActionBar();
			return true;
		}
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_edit_guardian) {
			startActivity(new Intent(this, UpdateGuardianActivity.class));
			return true;
		}
		if (id == R.id.action_edit_senior) {
			if (seniorLogin == null) {
				Toast.makeText(context,
						getString(R.string.error_senior_not_chosen),
						Toast.LENGTH_LONG).show();
			} else {
				startActivity(new Intent(this, UpdateSeniorActivity.class));
			}
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	/**
	 * A placeholder fragment containing a simple view.
	 */
	public static class PlaceholderFragment extends Fragment {
		/**
		 * The fragment argument representing the section number for this
		 * fragment.
		 */
		private static final String ARG_SECTION_NUMBER = "section_number";

		/**
		 * Returns a new instance of this fragment for the given section number.
		 */
		public static PlaceholderFragment newInstance(int sectionNumber) {
			PlaceholderFragment fragment = new PlaceholderFragment();
			Bundle args = new Bundle();
			args.putInt(ARG_SECTION_NUMBER, sectionNumber);
			fragment.setArguments(args);
			return fragment;
		}

		public PlaceholderFragment() {
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			View rootView = inflater.inflate(R.layout.fragment_main, container,
					false);
			return rootView;
		}

		@Override
		public void onAttach(Activity activity) {
			super.onAttach(activity);
			((MainActivity) activity).onSectionAttached(getArguments().getInt(
					ARG_SECTION_NUMBER));
		}
	}

}
