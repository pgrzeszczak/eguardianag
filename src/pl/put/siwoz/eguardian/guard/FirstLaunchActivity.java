package pl.put.siwoz.eguardian.guard;

import android.accounts.AccountManager;
import android.app.Activity;
import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

public class FirstLaunchActivity extends Activity {

	// Constants
	// The authority for the sync adapter's content provider
	public static final String ACCOUNT_TYPE = "example.com";
	public static AccountManager accountManager = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_first_launch);

		if (savedInstanceState == null) {
			getFragmentManager().beginTransaction()
					.add(R.id.container, new PlaceholderFragment()).commit();
		}

		accountManager = (AccountManager) this
				.getSystemService(ACCOUNT_SERVICE);

	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	public void logInButtonClick(View view) {
		startActivity(new Intent(this, LoginActivity.class));
	}

	public void registerButtonClick(View view) {
		startActivity(new Intent(this, RegisterActivity.class));
	}

	/**
	 * A placeholder fragment containing a simple view.
	 */
	public static class PlaceholderFragment extends Fragment {

		public PlaceholderFragment() {
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			View rootView = inflater.inflate(R.layout.fragment_first_launch,
					container, false);

			// if (accountManager.getAccountsByType(ACCOUNT_TYPE).length == 0) {
			// Button loginButton = (Button) rootView
			// .findViewById(R.id.login_button);
			// loginButton.setEnabled(false);
			// }
			return rootView;
		}

	}

}
