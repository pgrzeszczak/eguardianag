package pl.put.siwoz.eguardian.guard;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.KeyguardManager;
import android.app.KeyguardManager.KeyguardLock;
import android.content.Context;
import android.os.Bundle;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

public class AlarmNotificationActivity extends Activity {
	WakeLock wakeLock = null;
	KeyguardLock keyLock = null;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN);

		setContentView(R.layout.activity_alarm_notification);
		
		TextView text = (TextView)findViewById(R.id.textView_alarmData);
		try {
			JSONObject alarm = new JSONObject(getIntent().getStringExtra("alarm"));
			text.setText(alarm.getJSONObject("senior").getString("username"));
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		PowerManager powerManager = (PowerManager) getSystemService(Context.POWER_SERVICE);
	    wakeLock = powerManager.newWakeLock((PowerManager.SCREEN_BRIGHT_WAKE_LOCK | PowerManager.FULL_WAKE_LOCK | PowerManager.ACQUIRE_CAUSES_WAKEUP), "TAG");
	    wakeLock.acquire();

	    KeyguardManager keyguardManager = (KeyguardManager) getSystemService(Context.KEYGUARD_SERVICE);
	    keyLock = keyguardManager.newKeyguardLock("TAG");
	    keyLock.disableKeyguard();
	    runOnUiThread(new Runnable(){
	        public void run(){
	            getWindow().addFlags(
	                      WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED
	                    | WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON
	                    | WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON
	                    | WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD);                
	        }
	    });
	}
	
	@Override
	protected void onPause() {
		wakeLock.release();
		keyLock.reenableKeyguard();
		super.onPause();
	}
	
	public void understoodButtonClick(View view) {
		finish();
	}
}
