package pl.put.siwoz.eguardian.guard;

import pl.put.siwoz.eguardian.guard.backend.BackendService;
import pl.put.siwoz.eguardian.guard.dialogs.ProgressDialogStatic;
import pl.put.siwoz.eguardian.guard.entities.MedicineEvent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

public class AddMedicineEventFragment extends AddEventFragment {

	public int medicineId = 0;
	MedicineEvent preparedEvent = null;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_add_medicine, container,
				false);
		this.rootView = rootView;

		fillFieldsWithDefaults(rootView);

		eventId = getArguments() != null ? getArguments().getInt("eventId", -1)
				: -1;

		if (eventId != -1) {
			ProgressDialogStatic.show(getActivity());
			GetMedicineTask task = new GetMedicineTask();
			task.execute((Void) null);
			((Button) rootView.findViewById(R.id.button1))
					.setText("Zapisz zmiany");
			rootView.findViewById(R.id.button2).setVisibility(View.VISIBLE);
		}

		return rootView;
	}

	private class GetMedicineTask extends AsyncTask<Void, Void, Boolean> {

		private MedicineEvent event;

		@Override
		protected Boolean doInBackground(Void... params) {
			BackendService bs = new BackendService(getActivity());
			event = bs.getMedicineEvent(eventId);
			return true;
		}

		@Override
		protected void onPostExecute(final Boolean success) {
			fillFields(event);
			ProgressDialogStatic.hide();
		}

	}

	private class AddMedicineTask extends AsyncTask<Void, Void, Boolean> {

		@Override
		protected Boolean doInBackground(Void... params) {
			BackendService bs = new BackendService(getActivity());
			bs.addMedicineEvent(preparedEvent);
			return true;
		}

		@Override
		protected void onPostExecute(final Boolean success) {
			ProgressDialogStatic.hide();
			((MainActivity) getActivity()).openMedicineListFragment();
		}
	}

	private class DeleteMedicineTask extends AsyncTask<Void, Void, Boolean> {

		@Override
		protected Boolean doInBackground(Void... params) {
			BackendService bs = new BackendService(getActivity());
			bs.deleteMedicineEvent(eventId);
			return true;
		}

		@Override
		protected void onPostExecute(final Boolean success) {
			ProgressDialogStatic.hide();
			((MainActivity) getActivity()).openMedicineListFragment();
		}
	}

	private class EditMedicineTask extends AsyncTask<Void, Void, Boolean> {

		@Override
		protected Boolean doInBackground(Void... params) {
			BackendService bs = new BackendService(getActivity());
			bs.editMedicineEvent(preparedEvent);
			return true;
		}

		@Override
		protected void onPostExecute(final Boolean success) {
			ProgressDialogStatic.hide();
			((MainActivity) getActivity()).openMedicineListFragment();
		}
	}

	public void fillFields(MedicineEvent event) {

		medicineId = event.getMedicineId();

		EditText eventNameEditText = (EditText) rootView
				.findViewById(R.id.eventName);
		eventNameEditText.setText(event.getName());
		EditText eventDescriptionEditText = (EditText) rootView
				.findViewById(R.id.eventDescription);
		eventDescriptionEditText.setText(event.getDose());

		fillCommonEventFields(event);

	}

	public MedicineEvent getMedicineEventFromFields() {
		MedicineEvent event = new MedicineEvent();
		event.setId(eventId);

		String eventName = ((EditText) rootView.findViewById(R.id.eventName))
				.getText().toString();
		event.setName(eventName);
		String eventDose = ((EditText) rootView
				.findViewById(R.id.eventDescription)).getText().toString();
		event.setDose(eventDose);

		getCommontEventPropertiesFromFields(event);

		event.setMedicineId(medicineId);

		return event;
	}

	public void addMedicine(View view) {
		ProgressDialogStatic.show(getActivity());
		preparedEvent = getMedicineEventFromFields();
		if (eventId == -1) {
			AddMedicineTask task = new AddMedicineTask();
			task.execute((Void) null);
		} else {
			EditMedicineTask task = new EditMedicineTask();
			task.execute((Void) null);
		}
	}

	public void deleteMedicine(View view) {
		ProgressDialogStatic.show(getActivity());
		preparedEvent = getMedicineEventFromFields();
		DeleteMedicineTask task = new DeleteMedicineTask();
		task.execute((Void) null);
	}
}
