package pl.put.siwoz.eguardian.guard.json;

import org.json.JSONException;
import org.json.JSONObject;

import pl.put.siwoz.eguardian.guard.MainActivity;
import pl.put.siwoz.eguardian.guard.R;
import pl.put.siwoz.eguardian.guard.gcm.RegistrationIdRetriever;
import android.app.Activity;
import android.widget.EditText;

public abstract class UserJson {
	private static String getFromEditText(Activity activity, int id) {
		return ((EditText)activity.findViewById(id)).getText().toString();
	}
	private static void setToEditText(Activity activity, int id, String text) {
		((EditText)activity.findViewById(id)).setText(text);
	}
	public static JSONObject createJSONFromRegistrationForm(Activity activity) throws JSONException {
		JSONObject user = new JSONObject();
		JSONObject place = new JSONObject();
		place.put("street", getFromEditText(activity, R.id.street));
		place.put("street_no", getFromEditText(activity, R.id.street_no));
		place.put("apartment_no", getFromEditText(activity, R.id.apartament_no));
		place.put("city", getFromEditText(activity, R.id.city));
		place.put("post_code", getFromEditText(activity, R.id.post_code));
		place.put("telephone_number", getFromEditText(activity, R.id.telephone_number));
		user.put("username", getFromEditText(activity, R.id.login_register));
		user.put("password", getFromEditText(activity, R.id.password_register));
		user.put("place", place);
		user.put("registration_id", new RegistrationIdRetriever(activity).getRegistrationId());
		return user;
	}
	
	public static JSONObject createJSONFromUpdateForm(Activity activity) throws JSONException {
		JSONObject user = new JSONObject();
		JSONObject place = new JSONObject();
		place.put("street", getFromEditText(activity, R.id.street));
		place.put("street_no", getFromEditText(activity, R.id.street_no));
		place.put("apartment_no", getFromEditText(activity, R.id.apartament_no));
		place.put("city", getFromEditText(activity, R.id.city));
		place.put("post_code", getFromEditText(activity, R.id.post_code));
		place.put("telephone_number", getFromEditText(activity, R.id.telephone_number));
		user.put("username", MainActivity.guardianLogin);
		user.put("place", place);
		return user;
	}
	
	public static JSONObject createJSONFromSeniorUpdateForm(Activity activity) throws JSONException {
		JSONObject user = new JSONObject();
		JSONObject place = new JSONObject();
		place.put("street", getFromEditText(activity, R.id.street));
		place.put("street_no", getFromEditText(activity, R.id.street_no));
		place.put("apartment_no", getFromEditText(activity, R.id.apartament_no));
		place.put("city", getFromEditText(activity, R.id.city));
		place.put("post_code", getFromEditText(activity, R.id.post_code));
		place.put("telephone_number", getFromEditText(activity, R.id.telephone_number));
		user.put("username", MainActivity.seniorLogin);
		user.put("place", place);
		return user;
	}
	
	public static void createFormRegistrationFromJson(Activity activity, String json) throws JSONException {
		JSONObject user = new JSONObject(json);
		JSONObject place = user.getJSONObject("place");
		setToEditText(activity, R.id.street, place.getString("street"));
		setToEditText(activity, R.id.street_no, place.getString("street_no"));
		setToEditText(activity, R.id.apartament_no, place.getString("apartment_no"));
		setToEditText(activity, R.id.city, place.getString("city"));
		setToEditText(activity, R.id.post_code, place.getString("post_code"));
		setToEditText(activity, R.id.telephone_number, place.getString("telephone_number"));
	}
	
	public static JSONObject createJSONFromRegistrationSeniorForm(Activity activity) throws JSONException {
		JSONObject user = new JSONObject();
		JSONObject place = new JSONObject();
		place.put("street", getFromEditText(activity, R.id.street));
		place.put("street_no", getFromEditText(activity, R.id.street_no));
		place.put("apartment_no", getFromEditText(activity, R.id.apartament_no));
		place.put("city", getFromEditText(activity, R.id.city));
		place.put("post_code", getFromEditText(activity, R.id.post_code));
		place.put("telephone_number", getFromEditText(activity, R.id.telephone_number));
		user.put("username", getFromEditText(activity, R.id.login_register_senior));
		user.put("password", getFromEditText(activity, R.id.password_register_senior));
		user.put("place", place);
		user.put("registration_id", new RegistrationIdRetriever(activity).getRegistrationId());
		return user;
	}
}
