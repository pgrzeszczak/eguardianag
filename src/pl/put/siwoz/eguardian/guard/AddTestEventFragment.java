package pl.put.siwoz.eguardian.guard;

import pl.put.siwoz.eguardian.guard.backend.BackendService;
import pl.put.siwoz.eguardian.guard.dialogs.ProgressDialogStatic;
import pl.put.siwoz.eguardian.guard.entities.TestEvent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

public class AddTestEventFragment extends AddEventFragment {

	public int testId = 0;
	TestEvent preparedEvent = null;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_add_test, container,
				false);
		this.rootView = rootView;

		fillFieldsWithDefaults(rootView);

		eventId = getArguments() != null ? getArguments().getInt("eventId", -1)
				: -1;

		if (eventId != -1) {
			ProgressDialogStatic.show(getActivity());
			GetTestTask task = new GetTestTask();
			task.execute((Void) null);
			((Button) rootView.findViewById(R.id.button1))
					.setText("Zapisz zmiany");
			rootView.findViewById(R.id.button2).setVisibility(View.VISIBLE);
		}

		return rootView;
	}

	private class GetTestTask extends AsyncTask<Void, Void, Boolean> {

		private TestEvent event;

		@Override
		protected Boolean doInBackground(Void... params) {
			BackendService bs = new BackendService(getActivity());
			event = bs.getTestEvent(eventId);
			return true;
		}

		@Override
		protected void onPostExecute(final Boolean success) {
			fillFields(event);
			ProgressDialogStatic.hide();
		}

	}

	private class AddTestTask extends AsyncTask<Void, Void, Boolean> {

		@Override
		protected Boolean doInBackground(Void... params) {
			BackendService bs = new BackendService(getActivity());
			bs.addTestEvent(preparedEvent);
			return true;
		}

		@Override
		protected void onPostExecute(final Boolean success) {
			ProgressDialogStatic.hide();
			((MainActivity) getActivity()).openTestListFragment();
		}
	}

	private class DeleteTestTask extends AsyncTask<Void, Void, Boolean> {

		@Override
		protected Boolean doInBackground(Void... params) {
			BackendService bs = new BackendService(getActivity());
			bs.deleteTestEvent(eventId);
			return true;
		}

		@Override
		protected void onPostExecute(final Boolean success) {
			ProgressDialogStatic.hide();
			((MainActivity) getActivity()).openTestListFragment();
		}
	}

	private class EditTestTask extends AsyncTask<Void, Void, Boolean> {

		@Override
		protected Boolean doInBackground(Void... params) {
			BackendService bs = new BackendService(getActivity());
			bs.editTestEvent(preparedEvent);
			return true;
		}

		@Override
		protected void onPostExecute(final Boolean success) {
			ProgressDialogStatic.hide();
			((MainActivity) getActivity()).openTestListFragment();
		}
	}

	public void fillFields(TestEvent event) {

		testId = event.getTestId();

		EditText eventNameEditText = (EditText) rootView
				.findViewById(R.id.eventName);
		eventNameEditText.setText(event.getName());

		fillCommonEventFields(event);

	}

	public TestEvent getTestEventFromFields() {
		TestEvent event = new TestEvent();
		event.setId(eventId);

		String eventName = ((EditText) rootView.findViewById(R.id.eventName))
				.getText().toString();
		event.setName(eventName);

		getCommontEventPropertiesFromFields(event);

		event.setTestId(testId);

		return event;
	}

	public void addTest(View view) {
		ProgressDialogStatic.show(getActivity());
		preparedEvent = getTestEventFromFields();
		if (eventId == -1) {
			AddTestTask task = new AddTestTask();
			task.execute((Void) null);
		} else {
			EditTestTask task = new EditTestTask();
			task.execute((Void) null);
		}
	}

	public void deleteTest(View view) {
		ProgressDialogStatic.show(getActivity());
		preparedEvent = getTestEventFromFields();
		DeleteTestTask task = new DeleteTestTask();
		task.execute((Void) null);
	}
}
