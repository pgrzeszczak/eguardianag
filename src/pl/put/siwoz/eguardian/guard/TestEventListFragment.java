package pl.put.siwoz.eguardian.guard;

import java.util.ArrayList;

import pl.put.siwoz.eguardian.guard.backend.BackendService;
import pl.put.siwoz.eguardian.guard.dialogs.ProgressDialogStatic;
import pl.put.siwoz.eguardian.guard.entities.TestEvent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

public class TestEventListFragment extends EventListFragment {

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_test_list,
				container, false);
		ProgressDialogStatic.show(getActivity());
		return rootView;
	}

	public void getTestList() {
		GetTestListTask task = new GetTestListTask();
		task.execute((Void) null);
	}

	public void updateTestList(ArrayList<TestEvent> list) {
		events.clear();
		events.addAll(list);
		EventListAdapter adapter = new EventListAdapter(getActivity());
		setListAdapter(adapter);
		ProgressDialogStatic.hide();
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

		getTestList();
	}

	@Override
	public void onListItemClick(ListView l, View v, int position, long id) {
		AddEventFragment addEventFragment = new AddTestEventFragment();
		openAddEventFragment(addEventFragment, position);

	}

	public class GetTestListTask extends AsyncTask<Void, Void, Boolean> {

		ArrayList<TestEvent> list;

		@Override
		protected Boolean doInBackground(Void... params) {
			BackendService bs = new BackendService(getActivity());
			list = bs.getAllTestEvents();
			return true;
		}

		@Override
		protected void onPostExecute(final Boolean success) {
			updateTestList(list);
		}

	}
}
