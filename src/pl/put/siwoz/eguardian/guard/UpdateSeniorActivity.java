package pl.put.siwoz.eguardian.guard;

import org.json.JSONException;
import org.json.JSONObject;

import pl.put.siwoz.eguardian.guard.backend.BackendService;
import pl.put.siwoz.eguardian.guard.dialogs.ProgressDialogStatic;
import pl.put.siwoz.eguardian.guard.json.UserJson;
import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.app.Activity;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

/**
 * Activity which displays a register screen to the user, offering registration
 * as well.
 */
public class UpdateSeniorActivity extends Activity {
	/**
	 * A dummy authentication store containing known user names and passwords.
	 * TODO: remove after connecting to a real authentication system.
	 */
	private static final String[] DUMMY_CREDENTIALS = new String[] {
			"foo@example.com:hello", "bar@example.com:world" };

	/**
	 * The default email to populate the email field with.
	 */
	public static final String EXTRA_EMAIL = "com.example.android.authenticatordemo.extra.EMAIL";

	public static final String TAG = "UpdateSeniorActivity";

	/**
	 * Keep track of the register task to ensure we can cancel it if requested.
	 */
	private UserUpdateTask mAuthTask = null;

	private View mRegisterFormView;
	private View mRegisterStatusView;
	private TextView mRegisterStatusMessageView;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_update_senior);

		mRegisterFormView = findViewById(R.id.register_form_senior);
		mRegisterStatusView = findViewById(R.id.register_status_senior);
		mRegisterStatusMessageView = (TextView) findViewById(R.id.register_status_message_senior);

		findViewById(R.id.sign_in_button).setOnClickListener(
				new View.OnClickListener() {
					@Override
					public void onClick(View view) {
						attemptUpdate();
					}
				});

		prepareForm();
	}

	public void prepareForm() {
		ProgressDialogStatic.show(this);
		new AsyncTask<Activity, Void, String>() {

			Activity ac;

			@Override
			protected String doInBackground(Activity... params) {
				ac = params[0];
				Log.d("ASASASASASASA", MainActivity.seniorLogin);
				return new BackendService(ac)
						.getUserData(MainActivity.seniorLogin);
			}

			@Override
			protected void onPostExecute(String json) {
				try {
					UserJson.createFormRegistrationFromJson(ac, json);
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				ProgressDialogStatic.hide();
			}
		}.execute(this);
	}

	/**
	 * Attempts to sign in or register the account specified by the register
	 * form. If there are form errors (invalid email, missing fields, etc.), the
	 * errors are presented and no actual register attempt is made.
	 */
	public void attemptUpdate() {
		if (mAuthTask != null) {
			return;
		}
		mRegisterStatusMessageView.setText(R.string.update_progress);
		showProgress(true);
		mAuthTask = new UserUpdateTask(this);
		mAuthTask.execute((Void) null);
	}

	/**
	 * Shows the progress UI and hides the register form.
	 */
	@TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
	private void showProgress(final boolean show) {
		// On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
		// for very easy animations. If available, use these APIs to fade-in
		// the progress spinner.
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
			int shortAnimTime = getResources().getInteger(
					android.R.integer.config_shortAnimTime);

			mRegisterStatusView.setVisibility(View.VISIBLE);
			mRegisterStatusView.animate().setDuration(shortAnimTime)
					.alpha(show ? 1 : 0)
					.setListener(new AnimatorListenerAdapter() {
						@Override
						public void onAnimationEnd(Animator animation) {
							mRegisterStatusView
									.setVisibility(show ? View.VISIBLE
											: View.GONE);
						}
					});

			mRegisterFormView.setVisibility(View.VISIBLE);
			mRegisterFormView.animate().setDuration(shortAnimTime)
					.alpha(show ? 0 : 1)
					.setListener(new AnimatorListenerAdapter() {
						@Override
						public void onAnimationEnd(Animator animation) {
							mRegisterFormView.setVisibility(show ? View.GONE
									: View.VISIBLE);
						}
					});
		} else {
			// The ViewPropertyAnimator APIs are not available, so simply show
			// and hide the relevant UI components.
			mRegisterStatusView.setVisibility(show ? View.VISIBLE : View.GONE);
			mRegisterFormView.setVisibility(show ? View.GONE : View.VISIBLE);
		}
	}

	/**
	 * Represents an asynchronous register/registration task used to
	 * authenticate the user.
	 */
	public class UserUpdateTask extends AsyncTask<Void, Void, Boolean> {

		private Activity activity;

		public UserUpdateTask(Activity activity) {
			super();
			this.activity = activity;
		}

		@Override
		protected Boolean doInBackground(Void... params) {
			// TODO: attempt authentication against a network service.

			try {
				// Simulate network access.
				JSONObject user = UserJson.createJSONFromSeniorUpdateForm(activity);
				Log.d(TAG, user.toString(2));
				Log.d("jsss", user.toString());
				Thread.sleep(2000);
				BackendService backendService = new BackendService(
						this.activity);
				backendService.updateSenior(user.toString());
			} catch (InterruptedException | JSONException e) {
				return false;
			}
			return true;
		}

		@Override
		protected void onPostExecute(final Boolean success) {
			showProgress(false);

			if (success) {
				activity.finish();
			} else {
				// blad
			}
		}

		@Override
		protected void onCancelled() {
			mAuthTask = null;
			showProgress(false);
		}
	}
}
