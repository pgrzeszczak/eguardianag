package pl.put.siwoz.eguardian.guard;

import java.util.ArrayList;

import pl.put.siwoz.eguardian.guard.backend.BackendService;
import pl.put.siwoz.eguardian.guard.dialogs.ProgressDialogStatic;
import pl.put.siwoz.eguardian.guard.entities.MedicineEvent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

public class MedicineEventListFragment extends EventListFragment {

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_medicine_list,
				container, false);
		ProgressDialogStatic.show(getActivity());
		return rootView;
	}

	public void getMedicineList() {
		GetMedicineListTask task = new GetMedicineListTask();
		task.execute((Void) null);
	}

	public void updateMedicineList(ArrayList<MedicineEvent> list) {
		events.clear();
		events.addAll(list);
		EventListAdapter adapter = new EventListAdapter(getActivity());
		setListAdapter(adapter);
		ProgressDialogStatic.hide();
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

		getMedicineList();
	}

	@Override
	public void onListItemClick(ListView l, View v, int position, long id) {
		AddEventFragment addEventFragment = new AddMedicineEventFragment();
		openAddEventFragment(addEventFragment, position);

	}

	public class GetMedicineListTask extends AsyncTask<Void, Void, Boolean> {

		ArrayList<MedicineEvent> list;

		@Override
		protected Boolean doInBackground(Void... params) {
			BackendService bs = new BackendService(getActivity());
			list = bs.getAllMedicineEvents();
			return true;
		}

		@Override
		protected void onPostExecute(final Boolean success) {
			updateMedicineList(list);
		}

	}
}
