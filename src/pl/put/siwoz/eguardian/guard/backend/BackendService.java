package pl.put.siwoz.eguardian.guard.backend;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import pl.put.siwoz.eguardian.guard.MainActivity;
import pl.put.siwoz.eguardian.guard.entities.Event;
import pl.put.siwoz.eguardian.guard.entities.MealEvent;
import pl.put.siwoz.eguardian.guard.entities.MedicineEvent;
import pl.put.siwoz.eguardian.guard.entities.TestEvent;
import pl.put.siwoz.eguardian.guard.entities.User;
import pl.put.siwoz.eguardian.guard.entities.VisitEvent;
import pl.put.siwoz.eguardian.guard.utils.ISO8601;
import android.app.Activity;
import android.os.AsyncTask;
import android.util.Base64;
import android.util.Log;

public class BackendService {

	private Activity activity;

	public BackendService(Activity activity) {
		super();
		this.activity = activity;
	}

	public static int LOGIN_SUCCESSFUL = 0;
	public static int LOGIN_FAILED = 1;

	public static String address = "http://69.vot.pl/eguardian/web/app_dev.php";
	public static String mLogin;
	public static String mPassword;

	private String getBasicAuthPropertyString()
			throws UnsupportedEncodingException {
		byte[] data = null;
		String userCredentials = mLogin + ":" + mPassword;
		// String userCredentials = "guardianLogin:123";
		data = userCredentials.getBytes("UTF-8");

		return "Basic " + Base64.encodeToString(data, Base64.NO_WRAP);
	}

	private HttpURLConnection getUrlConnection(String path) throws IOException {
		return getUrlConnection(path, true);
	}

	private HttpURLConnection getUrlConnection(String path, boolean useBasicAuth)
			throws IOException {
		URL url = new URL(address + path);
		HttpURLConnection urlConnection = (HttpURLConnection) url
				.openConnection();
		if (useBasicAuth) {
			urlConnection.setRequestProperty("Authorization",
					getBasicAuthPropertyString());
			urlConnection.setUseCaches(false);
		}

		return urlConnection;
	}

	public void setCredentials(String mLogin, String mPassword) {
		BackendService.mLogin = mLogin;
		BackendService.mPassword = mPassword;
	}

	public int loginGuardian() {
		try {
			HttpURLConnection urlConnection = getUrlConnection("/login/guardian");
			if (urlConnection.getResponseCode() == 200)
				return LOGIN_SUCCESSFUL;
		} catch (IOException e) {
			e.printStackTrace();
		}
		return LOGIN_FAILED;

	}

	public int getUserId(String login) {
		String user = getUserData(login);
		JSONObject userJ;
		try {
			userJ = new JSONObject(user);
			return userJ.getInt("id");
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return -1;
	}

	public int getSeniorId() {
		return getUserId(MainActivity.seniorLogin);
	}

	public int getGuardianId() {
		return getUserId(MainActivity.guardianLogin);
	}

	public String getUserData(String login) {
		try {
			HttpURLConnection urlConnection = getUrlConnection("/user/" + login);
			return getResponseString(urlConnection);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	public ArrayList<User> getAllSeniors() {
		ArrayList<User> list = new ArrayList<User>();
		try {
			HttpURLConnection urlConnection = getUrlConnection("/login/guardian");
			list = getSeniorsFromJSON(getResponseString(urlConnection));
			urlConnection.getResponseCode();
		} catch (IOException | JSONException e) {
			e.printStackTrace();
		}
		return list;
	}

	public void getAllSeniors(final ResponseFinishedCallback callback) {
		new AsyncTask<Void, Void, List<User>>() {
			@Override
			protected List<User> doInBackground(Void... params) {
				return getAllSeniors();
			}

			@Override
			protected void onPostExecute(final List<User> list) {
				activity.runOnUiThread(new Runnable() {
					@Override
					public void run() {
						callback.onPostExecute(list);
					}
				});
			}
		}.execute();
	}

	public void registerGuardian(String json) {
		try {
			HttpURLConnection urlConnection = getUrlConnection("/user/guardian");
			putJSON(urlConnection, json);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void updateRegistrationId(String registrationId) {
		try {
			HttpURLConnection urlConnection = getUrlConnection("/user/registrationId");
			String json = "{\"RegistrationId\": \"" + registrationId + "\"}";
			postJSON(urlConnection, json);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void updateSenior(String json) {
		try {
			HttpURLConnection urlConnection = getUrlConnection("/user/senior");
			postJSON(urlConnection, json);
			Log.d("ASAFASDFDSFFSD", urlConnection.getResponseCode() + "");
			Log.d("ASAFASDFDSFFSD", getResponseString(urlConnection));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void updateGuardian(String json) {
		try {
			HttpURLConnection urlConnection = getUrlConnection("/user/guardian");
			postJSON(urlConnection, json);
			Log.d("ASAFASDFDSFFSD", urlConnection.getResponseCode() + "");
			Log.d("ASAFASDFDSFFSD", getResponseString(urlConnection));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void registerSenior(String json) {
		try {
			HttpURLConnection urlConnection = getUrlConnection("/user/senior");
			putJSON(urlConnection, json);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public boolean addSeniorToGuardian(String seniorLogin, String guardianLogin) {
		try {
			Log.d("Adding senior", seniorLogin + " on " + "/user/"
					+ guardianLogin + "/" + seniorLogin);
			HttpURLConnection urlConnection = getUrlConnection("/user/"
					+ guardianLogin + "/" + seniorLogin);
			putJSON(urlConnection, "");
			Log.d("adding senior", urlConnection.getResponseCode() + "");
			if (urlConnection.getResponseCode() == 200) {
				return true;
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return false;
	}

	private void putJSON(HttpURLConnection urlConnection, String json)
			throws IOException {
		urlConnection.setRequestMethod("PUT");
		sendJSON(urlConnection, json);
	}

	private void postJSON(HttpURLConnection urlConnection, String json)
			throws IOException {
		urlConnection.setRequestMethod("POST");
		sendJSON(urlConnection, json);
	}

	private void sendJSON(HttpURLConnection urlConnection, String json)
			throws IOException {
		urlConnection.setRequestProperty("Content-Type",
				"application/json; charset=utf8");
		OutputStream os = urlConnection.getOutputStream();
		os.write(json.getBytes("UTF-8"));
		os.close();
		int code = urlConnection.getResponseCode();
		Log.d("sendJSON", "" + json);
		Log.d("sendJSON", "" + code);
		Log.d("sendJSON", getResponseString(urlConnection));

	}

	private ArrayList<User> getSeniorsFromJSON(String string)
			throws JSONException {
		ArrayList<User> list = new ArrayList<User>();

		JSONObject jsonn = new JSONObject(string);
		string = jsonn.getJSONArray("seniors").toString();

		JSONArray json = new JSONArray(string);
		for (int x = 0; x < json.length(); x++) {
			JSONObject user = json.getJSONObject(x);
			int id = user.getInt("id");
			String username = user.getString("username");
			Log.d("json", "Senior " + id + ": " + username);
			User senior = new User().setLogin(username);
			list.add(senior);
		}

		return list;
	}

	private String getResponseString(HttpURLConnection urlConnection)
			throws IOException {
		BufferedReader in = new BufferedReader(new InputStreamReader(
				urlConnection.getInputStream()));
		String output;
		output = in.readLine();
		in.close();
		return output;
	}

	// *****************\
	// MealEventy
	// *****************/

	public ArrayList<MealEvent> getAllMealEvents() {
		ArrayList<MealEvent> list = new ArrayList<MealEvent>();
		try {
			HttpURLConnection urlConnection = getUrlConnection("/event/meal");
			list = getMealEventsFromJSON(getResponseString(urlConnection));
		} catch (IOException | JSONException e) {
			e.printStackTrace();
		}
		list = filterCorrectEvents(list);
		return list;
	}

	public MealEvent getMealEvent(int eventId) {
		MealEvent event = null;
		try {
			HttpURLConnection urlConnection = getUrlConnection("/event/meal/"
					+ eventId);
			event = getMealEventFromJSON(getResponseString(urlConnection));

		} catch (IOException | JSONException e) {
			e.printStackTrace();
		}
		return event;
	}

	public void addMealEvent(MealEvent event) {
		try {
			HttpURLConnection urlConnection = getUrlConnection("/event/meal");
			String json = generateJSONForMealEvent(event).toString();
			putJSON(urlConnection, json);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void deleteMealEvent(int eventId) {
		deleteEvent("meal", eventId);
	}

	public void editMealEvent(MealEvent event) {
		try {
			HttpURLConnection urlConnection = getUrlConnection("/event/meal");
			String json = generateJSONForMealEvent(event).toString();
			postJSON(urlConnection, json);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private JSONObject generateJSONForMealEvent(MealEvent event) {
		JSONObject json = new JSONObject();

		try {
			json.put("discr", "meal");

			JSONObject meal = new JSONObject();
			meal.put("id", event.getMealId());
			meal.put("name", event.getName());
			meal.put("description", event.getDescription());
			json.put("meal", meal);

			updateJSONWithCommonEventFields(json, event);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return json;
	}

	private MealEvent getMealEventFromJSON(String responseString)
			throws JSONException {

		MealEvent event = new MealEvent();
		JSONObject json = new JSONObject(responseString);

		JSONObject meal = json.getJSONObject("meal");
		event.setMealId(Integer.parseInt(meal.getString("id")));
		event.setName(meal.getString("name"));
		event.setDescription(meal.getString("description"));

		getCommonEventFieldsFromJSON(event, json);

		return event;
	}

	private ArrayList<MealEvent> getMealEventsFromJSON(String string)
			throws JSONException {
		ArrayList<MealEvent> list = new ArrayList<MealEvent>();

		JSONArray json = new JSONArray(string);
		for (int x = 0; x < json.length(); x++) {
			MealEvent event = getMealEventFromJSON(json.getJSONObject(x)
					.toString());
			list.add(event);
		}

		return list;
	}

	// *****************\
	// MedicineEventy
	// *****************/

	public ArrayList<MedicineEvent> getAllMedicineEvents() {
		ArrayList<MedicineEvent> list = new ArrayList<MedicineEvent>();
		try {
			HttpURLConnection urlConnection = getUrlConnection("/event/medicine");
			list = getMedicineEventsFromJSON(getResponseString(urlConnection));
		} catch (IOException | JSONException e) {
			e.printStackTrace();
		}
		list = filterCorrectEvents(list);
		return list;
	}

	public MedicineEvent getMedicineEvent(int eventId) {
		MedicineEvent event = null;
		try {
			HttpURLConnection urlConnection = getUrlConnection("/event/medicine/"
					+ eventId);
			event = getMedicineEventFromJSON(getResponseString(urlConnection));

		} catch (IOException | JSONException e) {
			e.printStackTrace();
		}
		return event;
	}

	public void addMedicineEvent(MedicineEvent event) {
		try {
			HttpURLConnection urlConnection = getUrlConnection("/event/medicine");
			String json = generateJSONForMedicineEvent(event).toString();
			putJSON(urlConnection, json);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void deleteMedicineEvent(int eventId) {
		deleteEvent("medicine", eventId);
	}

	public void editMedicineEvent(MedicineEvent event) {
		try {
			HttpURLConnection urlConnection = getUrlConnection("/event/medicine");
			String json = generateJSONForMedicineEvent(event).toString();
			postJSON(urlConnection, json);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private JSONObject generateJSONForMedicineEvent(MedicineEvent event) {
		JSONObject json = new JSONObject();

		try {
			json.put("discr", "medicine");

			JSONObject medicine = new JSONObject();
			medicine.put("id", event.getMedicineId());
			// medicine.put("category", event.getCategoryId());
			medicine.put("name", event.getName());
			medicine.put("dose", event.getDose());
			json.put("medicine", medicine);

			updateJSONWithCommonEventFields(json, event);

		} catch (JSONException e) {
			e.printStackTrace();
		}
		return json;
	}

	private MedicineEvent getMedicineEventFromJSON(String responseString)
			throws JSONException {

		MedicineEvent event = new MedicineEvent();
		JSONObject json = new JSONObject(responseString);

		JSONObject medicine = json.getJSONObject("medicine");
		event.setMedicineId(Integer.parseInt(medicine.getString("id")));
		// event.setCategoryId(Integer.parseInt(medicine.getString("category")));
		event.setName(medicine.getString("name"));
		event.setDose(medicine.getString("dose"));

		getCommonEventFieldsFromJSON(event, json);

		return event;
	}

	private ArrayList<MedicineEvent> getMedicineEventsFromJSON(String string)
			throws JSONException {
		ArrayList<MedicineEvent> list = new ArrayList<MedicineEvent>();

		JSONArray json = new JSONArray(string);
		for (int x = 0; x < json.length(); x++) {
			MedicineEvent event = getMedicineEventFromJSON(json
					.getJSONObject(x).toString());
			list.add(event);
		}

		return list;
	}

	// *****************\
	// TestEventy
	// *****************/

	public ArrayList<TestEvent> getAllTestEvents() {
		ArrayList<TestEvent> list = new ArrayList<TestEvent>();
		try {
			HttpURLConnection urlConnection = getUrlConnection("/event/test");
			list = getTestEventsFromJSON(getResponseString(urlConnection));
		} catch (IOException | JSONException e) {
			e.printStackTrace();
		}
		list = filterCorrectEvents(list);
		return list;
	}

	public TestEvent getTestEvent(int eventId) {
		TestEvent event = null;
		try {
			HttpURLConnection urlConnection = getUrlConnection("/event/test/"
					+ eventId);
			event = getTestEventFromJSON(getResponseString(urlConnection));

		} catch (IOException | JSONException e) {
			e.printStackTrace();
		}
		return event;
	}

	public void addTestEvent(TestEvent event) {
		try {
			HttpURLConnection urlConnection = getUrlConnection("/event/test");
			String json = generateJSONForTestEvent(event).toString();
			putJSON(urlConnection, json);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void deleteTestEvent(int eventId) {
		deleteEvent("test", eventId);
	}

	public void editTestEvent(TestEvent event) {
		try {
			HttpURLConnection urlConnection = getUrlConnection("/event/test");
			String json = generateJSONForTestEvent(event).toString();
			postJSON(urlConnection, json);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private JSONObject generateJSONForTestEvent(TestEvent event) {
		JSONObject json = new JSONObject();

		try {
			json.put("discr", "test");

			JSONObject test = new JSONObject();
			test.put("id", event.getTestId());
			// test.put("category", event.getCategoryId());
			test.put("name", event.getName());
			json.put("test", test);

			updateJSONWithCommonEventFields(json, event);

		} catch (JSONException e) {
			e.printStackTrace();
		}
		return json;
	}

	private TestEvent getTestEventFromJSON(String responseString)
			throws JSONException {

		TestEvent event = new TestEvent();
		JSONObject json = new JSONObject(responseString);

		JSONObject test = json.getJSONObject("test");
		event.setTestId(Integer.parseInt(test.getString("id")));
		// event.setCategoryId(Integer.parseInt(test.getString("category")));
		event.setName(test.getString("name"));

		getCommonEventFieldsFromJSON(event, json);

		return event;
	}

	private ArrayList<TestEvent> getTestEventsFromJSON(String string)
			throws JSONException {
		ArrayList<TestEvent> list = new ArrayList<TestEvent>();

		JSONArray json = new JSONArray(string);
		for (int x = 0; x < json.length(); x++) {
			TestEvent event = getTestEventFromJSON(json.getJSONObject(x)
					.toString());
			list.add(event);
		}

		return list;
	}

	// *****************\
	// VisitEventy
	// *****************/

	public ArrayList<VisitEvent> getAllVisitEvents() {
		ArrayList<VisitEvent> list = new ArrayList<VisitEvent>();
		try {
			HttpURLConnection urlConnection = getUrlConnection("/event/visit");
			list = getVisitEventsFromJSON(getResponseString(urlConnection));
		} catch (IOException | JSONException e) {
			e.printStackTrace();
		}
		list = filterCorrectEvents(list);
		return list;
	}

	public VisitEvent getVisitEvent(int eventId) {
		VisitEvent event = null;
		try {
			HttpURLConnection urlConnection = getUrlConnection("/event/visit/"
					+ eventId);
			event = getVisitEventFromJSON(getResponseString(urlConnection));

		} catch (IOException | JSONException e) {
			e.printStackTrace();
		}
		return event;
	}

	public void addVisitEvent(VisitEvent event) {
		try {
			HttpURLConnection urlConnection = getUrlConnection("/event/visit");
			String json = generateJSONForVisitEvent(event).toString();
			putJSON(urlConnection, json);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void deleteVisitEvent(int eventId) {
		deleteEvent("visit", eventId);
	}

	public void editVisitEvent(VisitEvent event) {
		try {
			HttpURLConnection urlConnection = getUrlConnection("/event/visit");
			String json = generateJSONForVisitEvent(event).toString();
			postJSON(urlConnection, json);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private JSONObject generateJSONForVisitEvent(VisitEvent event) {
		JSONObject json = new JSONObject();

		try {
			json.put("discr", "visit");

			JSONObject visit = new JSONObject();
			visit.put("id", event.getVisitId());
			// visit.put("category", event.getCategoryId());
			visit.put("name", event.getName());
			visit.put("doctor_name", event.getDoctorName());
			json.put("visit", visit);

			updateJSONWithCommonEventFields(json, event);

		} catch (JSONException e) {
			e.printStackTrace();
		}
		return json;
	}

	private VisitEvent getVisitEventFromJSON(String responseString)
			throws JSONException {

		VisitEvent event = new VisitEvent();
		JSONObject json = new JSONObject(responseString);

		JSONObject visit = json.getJSONObject("visit");
		event.setVisitId(Integer.parseInt(visit.getString("id")));
		// event.setCategoryId(Integer.parseInt(visit.getString("category")));
		event.setName(visit.getString("name"));
		event.setDoctorName(visit.getString("doctor_name"));

		getCommonEventFieldsFromJSON(event, json);

		return event;
	}

	private ArrayList<VisitEvent> getVisitEventsFromJSON(String string)
			throws JSONException {
		ArrayList<VisitEvent> list = new ArrayList<VisitEvent>();

		JSONArray json = new JSONArray(string);
		for (int x = 0; x < json.length(); x++) {
			VisitEvent event = getVisitEventFromJSON(json.getJSONObject(x)
					.toString());
			list.add(event);
		}

		return list;
	}

	// *****************\
	// Ogolnie Eventy
	// *****************/

	private void updateJSONWithCommonEventFields(JSONObject json, Event event)
			throws JSONException {
		json.put("id", event.getId());

		JSONObject senior = new JSONObject();
		senior.put("id", getSeniorId());
		json.put("senior", senior);

		JSONObject guardian = new JSONObject();
		// TODO: Powinno ładować id aktualnie zalogowanego guardiana
		guardian.put("id", 6);
		json.put("guardian", guardian);

		Calendar startTimeCalendar = Calendar.getInstance();
		startTimeCalendar.setTimeInMillis(event.getStartTime().getTime());
		json.put("start_date", ISO8601.fromCalendar(startTimeCalendar));
		Calendar endTimeCalendar = Calendar.getInstance();
		endTimeCalendar.setTimeInMillis(event.getEndTime().getTime());
		json.put("end_date", ISO8601.fromCalendar(endTimeCalendar));
		json.put("interval", event.getInterval());
	}

	private void getCommonEventFieldsFromJSON(Event event, JSONObject json)
			throws JSONException {
		JSONObject senior = json.getJSONObject("senior");
		event.setSeniorId(Integer.parseInt(senior.getString("id")));

		JSONObject guardian = json.getJSONObject("guardian");
		event.setGuardianId(Integer.parseInt(guardian.getString("id")));

		event.setId(Integer.parseInt(json.getString("id")));

		Calendar startTimeCal = null;
		Calendar endTimeCal = null;
		try {
			startTimeCal = ISO8601.toCalendar(json.getString("start_date"));
			endTimeCal = ISO8601.toCalendar(json.getString("end_date"));
		} catch (ParseException e) {
			e.printStackTrace();
		}
		event.setStartTime(new java.sql.Date(startTimeCal.getTimeInMillis()));
		event.setEndTime(new java.sql.Date(endTimeCal.getTimeInMillis()));
		event.setInterval(Integer.parseInt(json.getString("interval")));
	}

	private void deleteEvent(String eventType, int eventId) {
		try {
			HttpURLConnection urlConnection = getUrlConnection("/event/"
					+ eventType + "/" + eventId);
			urlConnection.setRequestMethod("DELETE");
			urlConnection.getResponseCode();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private <T extends Event> ArrayList<T> filterCorrectEvents(ArrayList<T> list) {
		ArrayList<T> listCopy = new ArrayList<T>();
		listCopy.addAll(list);
		list.clear();
		for (T event : listCopy) {
			if (event.getSeniorId() == getSeniorId()) {
				list.add(event);
			}
		}
		return list;
	}

}
