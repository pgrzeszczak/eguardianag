package pl.put.siwoz.eguardian.guard.backend;

public interface ResponseFinishedCallback {
	public void onPostExecute(Object... params);
}
