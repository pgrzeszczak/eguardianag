package pl.put.siwoz.eguardian.guard;

import pl.put.siwoz.eguardian.guard.backend.BackendService;
import pl.put.siwoz.eguardian.guard.dialogs.ProgressDialogStatic;
import pl.put.siwoz.eguardian.guard.entities.VisitEvent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

public class AddVisitEventFragment extends AddEventFragment {

	public int visitId = 0;
	VisitEvent preparedEvent = null;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_add_visit,
				container, false);
		this.rootView = rootView;

		fillFieldsWithDefaults(rootView);

		eventId = getArguments() != null ? getArguments().getInt("eventId", -1)
				: -1;

		if (eventId != -1) {
			ProgressDialogStatic.show(getActivity());
			GetVisitTask task = new GetVisitTask();
			task.execute((Void) null);
			((Button) rootView.findViewById(R.id.button1))
					.setText("Zapisz zmiany");
			rootView.findViewById(R.id.button2).setVisibility(View.VISIBLE);
		}

		return rootView;
	}

	private class GetVisitTask extends AsyncTask<Void, Void, Boolean> {

		private VisitEvent event;

		@Override
		protected Boolean doInBackground(Void... params) {
			BackendService bs = new BackendService(getActivity());
			event = bs.getVisitEvent(eventId);
			return true;
		}

		@Override
		protected void onPostExecute(final Boolean success) {
			fillFields(event);
			ProgressDialogStatic.hide();
		}

	}

	private class AddVisitTask extends AsyncTask<Void, Void, Boolean> {

		@Override
		protected Boolean doInBackground(Void... params) {
			BackendService bs = new BackendService(getActivity());
			bs.addVisitEvent(preparedEvent);
			return true;
		}

		@Override
		protected void onPostExecute(final Boolean success) {
			ProgressDialogStatic.hide();
			((MainActivity) getActivity()).openVisitListFragment();
		}
	}

	private class DeleteVisitTask extends AsyncTask<Void, Void, Boolean> {

		@Override
		protected Boolean doInBackground(Void... params) {
			BackendService bs = new BackendService(getActivity());
			bs.deleteVisitEvent(eventId);
			return true;
		}

		@Override
		protected void onPostExecute(final Boolean success) {
			ProgressDialogStatic.hide();
			((MainActivity) getActivity()).openVisitListFragment();
		}
	}

	private class EditVisitTask extends AsyncTask<Void, Void, Boolean> {

		@Override
		protected Boolean doInBackground(Void... params) {
			BackendService bs = new BackendService(getActivity());
			bs.editVisitEvent(preparedEvent);
			return true;
		}

		@Override
		protected void onPostExecute(final Boolean success) {
			ProgressDialogStatic.hide();
			((MainActivity) getActivity()).openVisitListFragment();
		}
	}

	public void fillFields(VisitEvent event) {

		visitId = event.getVisitId();

		EditText eventNameEditText = (EditText) rootView
				.findViewById(R.id.eventName);
		eventNameEditText.setText(event.getName());
		EditText eventDescriptionEditText = (EditText) rootView
				.findViewById(R.id.eventDescription);
		eventDescriptionEditText.setText(event.getDoctorName());

		fillCommonEventFields(event);

	}

	public VisitEvent getVisitEventFromFields() {
		VisitEvent event = new VisitEvent();
		event.setId(eventId);

		String eventName = ((EditText) rootView.findViewById(R.id.eventName))
				.getText().toString();
		event.setName(eventName);
		String eventDoctorName = ((EditText) rootView
				.findViewById(R.id.eventDescription)).getText().toString();
		event.setDoctorName(eventDoctorName);

		getCommontEventPropertiesFromFields(event);

		event.setVisitId(visitId);

		return event;
	}

	public void addVisit(View view) {
		ProgressDialogStatic.show(getActivity());
		preparedEvent = getVisitEventFromFields();
		if (eventId == -1) {
			AddVisitTask task = new AddVisitTask();
			task.execute((Void) null);
		} else {
			EditVisitTask task = new EditVisitTask();
			task.execute((Void) null);
		}
	}

	public void deleteVisit(View view) {
		ProgressDialogStatic.show(getActivity());
		preparedEvent = getVisitEventFromFields();
		DeleteVisitTask task = new DeleteVisitTask();
		task.execute((Void) null);
	}
}
