package pl.put.siwoz.eguardian.guard;

import pl.put.siwoz.eguardian.guard.backend.BackendService;
import pl.put.siwoz.eguardian.guard.dialogs.ProgressDialogStatic;
import pl.put.siwoz.eguardian.guard.entities.MealEvent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

public class AddMealEventFragment extends AddEventFragment {

	public int mealId = 0;
	MealEvent preparedEvent = null;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_add_meal, container,
				false);
		this.rootView = rootView;

		fillFieldsWithDefaults(rootView);

		eventId = getArguments() != null ? getArguments().getInt("eventId", -1)
				: -1;

		if (eventId != -1) {
			ProgressDialogStatic.show(getActivity());
			GetMealTask task = new GetMealTask();
			task.execute((Void) null);
			((Button) rootView.findViewById(R.id.button1))
					.setText("Zapisz zmiany");
			rootView.findViewById(R.id.button2).setVisibility(View.VISIBLE);
		}

		return rootView;
	}

	private class GetMealTask extends AsyncTask<Void, Void, Boolean> {

		private MealEvent event;

		@Override
		protected Boolean doInBackground(Void... params) {
			BackendService bs = new BackendService(getActivity());
			event = bs.getMealEvent(eventId);
			return true;
		}

		@Override
		protected void onPostExecute(final Boolean success) {
			fillFields(event);
			ProgressDialogStatic.hide();
		}

	}

	private class AddMealTask extends AsyncTask<Void, Void, Boolean> {

		@Override
		protected Boolean doInBackground(Void... params) {
			BackendService bs = new BackendService(getActivity());
			bs.addMealEvent(preparedEvent);
			return true;
		}

		@Override
		protected void onPostExecute(final Boolean success) {
			ProgressDialogStatic.hide();
			((MainActivity) getActivity()).openMealListFragment();
		}
	}

	private class DeleteMealTask extends AsyncTask<Void, Void, Boolean> {

		@Override
		protected Boolean doInBackground(Void... params) {
			BackendService bs = new BackendService(getActivity());
			bs.deleteMealEvent(eventId);
			return true;
		}

		@Override
		protected void onPostExecute(final Boolean success) {
			ProgressDialogStatic.hide();
			((MainActivity) getActivity()).openMealListFragment();
		}
	}

	private class EditMealTask extends AsyncTask<Void, Void, Boolean> {

		@Override
		protected Boolean doInBackground(Void... params) {
			BackendService bs = new BackendService(getActivity());
			bs.editMealEvent(preparedEvent);
			return true;
		}

		@Override
		protected void onPostExecute(final Boolean success) {
			ProgressDialogStatic.hide();
			((MainActivity) getActivity()).openMealListFragment();
		}
	}

	public void fillFields(MealEvent event) {

		mealId = event.getMealId();

		EditText eventNameEditText = (EditText) rootView
				.findViewById(R.id.eventName);
		eventNameEditText.setText(event.getName());
		EditText eventDescriptionEditText = (EditText) rootView
				.findViewById(R.id.eventDescription);
		eventDescriptionEditText.setText(event.getDescription());

		fillCommonEventFields(event);

	}

	public MealEvent getMealEventFromFields() {
		MealEvent event = new MealEvent();
		event.setId(eventId);

		String eventName = ((EditText) rootView.findViewById(R.id.eventName))
				.getText().toString();
		event.setName(eventName);
		String eventDescription = ((EditText) rootView
				.findViewById(R.id.eventDescription)).getText().toString();
		event.setDescription(eventDescription);

		getCommontEventPropertiesFromFields(event);

		event.setMealId(mealId);

		return event;
	}

	public void addMeal(View view) {
		ProgressDialogStatic.show(getActivity());
		preparedEvent = getMealEventFromFields();
		if (eventId == -1) {
			AddMealTask task = new AddMealTask();
			task.execute((Void) null);
		} else {
			EditMealTask task = new EditMealTask();
			task.execute((Void) null);
		}
	}

	public void deleteMeal(View view) {
		ProgressDialogStatic.show(getActivity());
		preparedEvent = getMealEventFromFields();
		DeleteMealTask task = new DeleteMealTask();
		task.execute((Void) null);
	}
}
