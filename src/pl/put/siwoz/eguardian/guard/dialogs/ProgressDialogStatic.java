package pl.put.siwoz.eguardian.guard.dialogs;

import android.content.Context;

public class ProgressDialogStatic {
	private static MyProgressDialog dialog;
	
	public static void show(Context context) {
		dialog = MyProgressDialog.show(context, null, null, true);
	}
	
	public static void hide() {
		if (dialog != null) {
			dialog.dismiss();
			dialog = null;
		}
	}
}
