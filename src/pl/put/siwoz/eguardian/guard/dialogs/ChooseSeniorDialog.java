package pl.put.siwoz.eguardian.guard.dialogs;

import java.util.ArrayList;
import java.util.List;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.widget.ArrayAdapter;
import android.widget.Toast;
import pl.put.siwoz.eguardian.guard.AddSeniorActivity;
import pl.put.siwoz.eguardian.guard.MainActivity;
import pl.put.siwoz.eguardian.guard.R;
import pl.put.siwoz.eguardian.guard.backend.BackendService;
import pl.put.siwoz.eguardian.guard.backend.ResponseFinishedCallback;
import pl.put.siwoz.eguardian.guard.entities.User;

public class ChooseSeniorDialog {
	private BackendService backendService;
	private MainActivity mainActivity;
	private AlertDialog.Builder builderSingle;

	public ChooseSeniorDialog(MainActivity mainActivity) {
		super();
		this.mainActivity = mainActivity;
		builderSingle = new AlertDialog.Builder(mainActivity);
		backendService = new BackendService(mainActivity);
	}

	public ChooseSeniorDialog prepareAndShow() {
		ProgressDialogStatic.show(mainActivity);
		builderSingle.setIcon(R.drawable.ic_launcher);
		builderSingle.setTitle(mainActivity
				.getString(R.string.action_choose_senior));
		final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(
				mainActivity, android.R.layout.select_dialog_item);
		backendService.getAllSeniors(new ResponseFinishedCallback() {
			@Override
			public void onPostExecute(Object... params) {
				final List<User> seniors = (List<User>)params[0];
				for (User senior : seniors) {
					arrayAdapter.add(senior.toString());
				}
				arrayAdapter
						.add(mainActivity.getString(R.string.dialog_add_new_senior));
				builderSingle.setNegativeButton(
						mainActivity.getString(R.string.action_cancel),
						new DialogInterface.OnClickListener() {

							@Override
							public void onClick(DialogInterface dialog, int which) {
								dialog.dismiss();
							}
						});

				builderSingle.setAdapter(arrayAdapter,
						new DialogInterface.OnClickListener() {

							@Override
							public void onClick(DialogInterface dialog, int which) {
								if (which > seniors.size() - 1) {
									mainActivity.startActivity(new Intent(mainActivity,
											AddSeniorActivity.class));
								} else {
									User senior = seniors.get(which);
									MainActivity.seniorLogin = senior.getLogin();
								}
								dialog.dismiss();
							}
						});
				ProgressDialogStatic.hide();
				builderSingle.show();
			}
		});
		return this;
	}
}
