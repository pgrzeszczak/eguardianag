package pl.put.siwoz.eguardian.guard;

import java.sql.Date;
import java.util.Calendar;

import pl.put.siwoz.eguardian.guard.entities.Event;
import android.app.Activity;
import android.app.Fragment;
import android.view.View;
import android.widget.DatePicker;
import android.widget.NumberPicker;
import android.widget.TimePicker;

public class AddEventFragment extends Fragment {
	public int eventId;
	public View rootView;

	protected void fillFieldsWithDefaults(View rootView) {
		TimePicker tp1 = (TimePicker) rootView.findViewById(R.id.timePicker1);
		tp1.setIs24HourView(true);
		DatePicker dp1 = (DatePicker) rootView.findViewById(R.id.datePicker1);
		fillDatePickers(new Date(Calendar.getInstance().getTimeInMillis()),
				dp1, tp1);

		TimePicker tp2 = (TimePicker) rootView.findViewById(R.id.timePicker2);
		tp2.setIs24HourView(true);
		DatePicker dp2 = (DatePicker) rootView.findViewById(R.id.datePicker2);
		fillDatePickers(new Date(Calendar.getInstance().getTimeInMillis()),
				dp2, tp2);

		NumberPicker np1 = (NumberPicker) rootView
				.findViewById(R.id.numberPicker1);
		np1.setMaxValue(999);
		np1.setMinValue(0);
		np1.setValue(24);

		NumberPicker np2 = (NumberPicker) rootView
				.findViewById(R.id.numberPicker2);
		np2.setMaxValue(59);
		np2.setMinValue(0);
		np2.setValue(0);
	}

	protected void fillDatePickers(Date date, DatePicker dp, TimePicker tp) {
		Calendar cal = Calendar.getInstance();
		cal.setTimeInMillis(date.getTime());

		int day = cal.get(Calendar.DAY_OF_MONTH);
		int month = cal.get(Calendar.MONTH);
		int year = cal.get(Calendar.YEAR);
		int hour = cal.get(Calendar.HOUR_OF_DAY);
		int minutes = cal.get(Calendar.MINUTE);

		dp.updateDate(year, month, day);
		tp.setCurrentHour(hour);
		tp.setCurrentMinute(minutes);
	}

	protected Date getDateFromFields(DatePicker dp, TimePicker tp) {
		int year = dp.getYear();
		int month = dp.getMonth();
		int day = dp.getDayOfMonth();
		int hour = tp.getCurrentHour();
		int minutes = tp.getCurrentMinute();

		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.YEAR, year);
		cal.set(Calendar.MONTH, month);
		cal.set(Calendar.DAY_OF_MONTH, day);
		cal.set(Calendar.HOUR_OF_DAY, hour);
		cal.set(Calendar.MINUTE, minutes);
		return new Date(cal.getTimeInMillis());
	}

	protected void fillCommonEventFields(Event event) {
		DatePicker startTimeDatePicker = (DatePicker) rootView
				.findViewById(R.id.datePicker1);
		TimePicker startTimeTimePicker = (TimePicker) rootView
				.findViewById(R.id.timePicker1);
		fillDatePickers(event.getStartTime(), startTimeDatePicker,
				startTimeTimePicker);

		DatePicker endTimeDatePicker = (DatePicker) rootView
				.findViewById(R.id.datePicker2);
		TimePicker endTimeTimePicker = (TimePicker) rootView
				.findViewById(R.id.timePicker2);
		fillDatePickers(event.getEndTime(), endTimeDatePicker,
				endTimeTimePicker);

		int interval = event.getInterval();
		int intervalHours = interval / 60;
		int intervalMinutes = interval % 60;

		NumberPicker intervalHoursNumberPicker = (NumberPicker) rootView
				.findViewById(R.id.numberPicker1);
		intervalHoursNumberPicker.setValue(intervalHours);

		NumberPicker intervalMinutesNumberPicker = (NumberPicker) rootView
				.findViewById(R.id.numberPicker2);
		intervalMinutesNumberPicker.setValue(intervalMinutes);
	}

	protected void getCommontEventPropertiesFromFields(Event event) {
		int hoursInterval = ((NumberPicker) rootView
				.findViewById(R.id.numberPicker1)).getValue();
		int minutesInterval = ((NumberPicker) rootView
				.findViewById(R.id.numberPicker2)).getValue();
		int interval = hoursInterval * 60 + minutesInterval;
		event.setInterval(interval);

		DatePicker startTimeDatePicker = (DatePicker) rootView
				.findViewById(R.id.datePicker1);
		TimePicker startTimeTimePicker = (TimePicker) rootView
				.findViewById(R.id.timePicker1);
		event.setStartTime(getDateFromFields(startTimeDatePicker,
				startTimeTimePicker));

		DatePicker endTimeDatePicker = (DatePicker) rootView
				.findViewById(R.id.datePicker2);
		TimePicker endTimeTimePicker = (TimePicker) rootView
				.findViewById(R.id.timePicker2);
		event.setEndTime(getDateFromFields(endTimeDatePicker, endTimeTimePicker));
	}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		((MainActivity) activity).onSectionAttached(0);
	}
}
