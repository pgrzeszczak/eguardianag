package pl.put.siwoz.eguardian.guard;

import java.util.ArrayList;

import pl.put.siwoz.eguardian.guard.entities.Event;
import android.app.Activity;
import android.app.FragmentManager;
import android.app.ListFragment;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class EventListFragment extends ListFragment {
	protected ArrayList<Event> events = new ArrayList<Event>();

	protected class EventListAdapter extends BaseAdapter {
		private LayoutInflater inflater;

		public EventListAdapter(Activity activity) {
			inflater = (LayoutInflater) activity
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		}

		@Override
		public int getCount() {
			return events.size();
		}

		@Override
		public Object getItem(int position) {
			return events.get(position);
		}

		@Override
		public long getItemId(int position) {
			return events.get(position).getId();
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			View vi = convertView;

			if (convertView == null)
				vi = inflater.inflate(R.layout.meal_list_row, null);

			Event event = (Event) getItem(position);

			TextView tv1 = (TextView) vi.findViewById(R.id.textView1);
			TextView tv2 = (TextView) vi.findViewById(R.id.textView2);

			tv2.setText(event.getSomethingNamelike());
			tv1.setText(event.getHumanReadableInterval());

			return vi;
		}

	}

	protected void openAddEventFragment(AddEventFragment addEventFragment,
			int position) {
		((MainActivity) getActivity())
				.setCurrentAddEventFragment(addEventFragment);
		Bundle b = new Bundle();
		b.putInt("eventId", events.get(position).getId());
		addEventFragment.setArguments(b);
		FragmentManager fragmentManager = getFragmentManager();
		fragmentManager.beginTransaction()
				.replace(R.id.container, addEventFragment).commit();
	}
}
